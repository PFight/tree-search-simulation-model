﻿import { Model, Tree, Node, Object } from "../common/model";

export function printModel(model: Model): string {
    let result = "";

    result += "Tree count: " + model.trees.length + newLine;
    let nodeCount = 0;
    model.trees.forEach(tree => {
        tree.visitAllNodes(tree.root, (node) => nodeCount++);
    });
    result += "Nodes count: " + nodeCount + newLine;
    result += "Object count: " + model.objects.length + newLine;
    model.trees.forEach(tree => {
        result += "Tree " + tree.id;
        result += printNode(tree.root, model, 1);
        result += newLine;
    });

    return result;
}

export function objectToString(obj: Object): string {
    return obj.id + " (" + obj.nodes.map(x => x.id).join(", ") + ")";   
}

export function nodeToString(node: Node, showObjets: boolean = false): string {
    let result = getIdent(node.level) + node.id.toString();
    if (showObjets) result += " (" + node.getSelfObjects().length + " объектов)"; //+ node.getSelfObjects().map(x => x.id).join(", ") + ")";
    return result;
}

export function timeToString(time: number): string {
    return (time / 1000).toString() + " сек.";
}

export var newLine = "\r\n";

function printNode(currentNode: Node, model: Model, currentLevel: number): string {
    let result = newLine + currentNode.level.toString() + getIdent(currentLevel) + currentNode.id;
    let objects = currentNode.getSelfObjects().length + " объектов";  // model.objects.filter(x => x.nodes.indexOf(currentNode) >= 0).map(x => x.id).join(", ");
    result += " (" + objects + ")";

    if (currentNode.children) {
        for (let n = 0; n < currentNode.children.length; n++) {
            result += printNode(currentNode.children[n], model, currentLevel + 1);
        }
    }
    return result;
}

export function getIdent(size: number) {
    return (' ').repeat(size * 2);
}