"use strict";
function printModel(model) {
    let result = "";
    result += "Tree count: " + model.trees.length + exports.newLine;
    let nodeCount = 0;
    model.trees.forEach(tree => {
        tree.visitAllNodes(tree.root, (node) => nodeCount++);
    });
    result += "Nodes count: " + nodeCount + exports.newLine;
    result += "Object count: " + model.objects.length + exports.newLine;
    model.trees.forEach(tree => {
        result += "Tree " + tree.id;
        result += printNode(tree.root, model, 1);
        result += exports.newLine;
    });
    return result;
}
exports.printModel = printModel;
function objectToString(obj) {
    return obj.id + " (" + obj.nodes.map(x => x.id).join(", ") + ")";
}
exports.objectToString = objectToString;
function nodeToString(node, showObjets = false) {
    let result = getIdent(node.level) + node.id.toString();
    if (showObjets)
        result += " (" + node.getSelfObjects().length + " объектов)"; //+ node.getSelfObjects().map(x => x.id).join(", ") + ")";
    return result;
}
exports.nodeToString = nodeToString;
function timeToString(time) {
    return (time / 1000).toString() + " сек.";
}
exports.timeToString = timeToString;
exports.newLine = "\r\n";
function printNode(currentNode, model, currentLevel) {
    let result = exports.newLine + currentNode.level.toString() + getIdent(currentLevel) + currentNode.id;
    let objects = currentNode.getSelfObjects().length + " объектов"; // model.objects.filter(x => x.nodes.indexOf(currentNode) >= 0).map(x => x.id).join(", ");
    result += " (" + objects + ")";
    if (currentNode.children) {
        for (let n = 0; n < currentNode.children.length; n++) {
            result += printNode(currentNode.children[n], model, currentLevel + 1);
        }
    }
    return result;
}
function getIdent(size) {
    return (' ').repeat(size * 2);
}
exports.getIdent = getIdent;
//# sourceMappingURL=model-print.js.map