"use strict";
var time = 0;
function sleep(timeout) {
    if (timeout > 0) {
        time += timeout;
    }
}
exports.sleep = sleep;
function now() {
    return time;
}
exports.now = now;
//# sourceMappingURL=sleep.js.map