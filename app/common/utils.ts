﻿var idCounterMap = {};

export function generateId(prefix: string) {
    if (!idCounterMap[prefix]) {
        idCounterMap[prefix] = 0;
    }
    return prefix + idCounterMap[prefix]++;
}

export function resetIdGenerator() {
    idCounterMap = {};
}