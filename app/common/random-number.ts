﻿const randgen = require("randgen");

export interface RandomNumber {
    get(): number;
}

export class UniformRandomNumber implements RandomNumber {
    min: number;
    max: number;

    public constructor(min: number, max: number) {
        this.min = min;
        this.max = max;
    }

    public get(): number {
        return randgen.runif(this.min, this.max);
    }
}

export class NormalRandomNumber implements RandomNumber {
    med: number;
    dev: number;

    public constructor(med: number, dev: number) {
        this.med = med;
        this.dev = dev;
    }

    public get(): number {
        return randgen.rnorm(this.med, this.dev);
    }
}