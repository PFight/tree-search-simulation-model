﻿import { generateId } from "./utils";

export class Object {
    constructor() {
        this.id = generateId("obj");
    }
    id: string;
    nodes: Node[] = [];
}

export class Node {
    constructor(model: Model, level: number, parent?: Node) {
        this.id = generateId("node");
        this.parent = parent;
        this.level = level;
        this.model = model;
        if (parent) {
            parent.children.push(this);
            this.model.nodeCount++; // Root do not counts
        }
    }
    id: string;
    parent: Node;
    children: Node[] = [];
    level: number;
    model: Model;
    selfObjects: Object[] = [];
    get isLeaf(): boolean {
        return this.children.length == 0;
    }
    get isRoot(): boolean {
        return !this.parent;
    }

    get tree(): Tree {
        let root = this as Node;
        while (!root.isRoot) {
            root = root.parent;
        }
        return this.model.trees.find(x => x.root == root);
    }

    isParentOf(node: Node) {
        let parent = node.parent;
        while (parent) {
            if (parent == this) return true;
            parent = parent.parent;
        }
        return false;
    }

    getSelfObjects(): Object[] {
        return this.selfObjects;
    }
    getSelfAndChildrenObjects(): Object[] {
        return this.model.objects.filter(x => {
            return x.nodes.findIndex(node => node == this || this.isParentOf(node)) >= 0;
        });
    }
    getChildContaining(obj: Object): Node {
        const objects = this.getSelfAndChildrenObjects();
        if (objects.indexOf(obj) >= 0) {
            const containingNodeIndex = obj.nodes.findIndex(x => this.isParentOf(x));
            if (containingNodeIndex >= 0) {
                const containingNode = obj.nodes[containingNodeIndex];
                let node = containingNode;
                while (node.parent != this) {
                    node = node.parent;
                }
                return node;
            }
        }
        return null;
    }
}

export class Tree {
    constructor(model: Model) {
        this.id = generateId("tree");
        this.model = model;
    }
    id: string;
    root: Node;
    model: Model;

    visitAllNodes(current: Node, callback: (node: Node) => void) {
        callback(current);
        if (current.children) {
            current.children.forEach(x => this.visitAllNodes(x, callback));
        }
    }
}

export class Model {
    nodeCount: number = 0;

    trees: Tree[] = [];
    objects: Object[] = [];
}