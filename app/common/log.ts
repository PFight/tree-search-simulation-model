﻿var fs = require('fs');

let result = "";

export function log(text: string) {
    // console.info(text);
    result += text;
}

export function flush() {
    fs.writeFileSync("log.txt", result);
}