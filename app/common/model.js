"use strict";
const utils_1 = require("./utils");
class Object {
    constructor() {
        this.nodes = [];
        this.id = utils_1.generateId("obj");
    }
}
exports.Object = Object;
class Node {
    constructor(model, level, parent) {
        this.children = [];
        this.selfObjects = [];
        this.id = utils_1.generateId("node");
        this.parent = parent;
        this.level = level;
        this.model = model;
        if (parent) {
            parent.children.push(this);
            this.model.nodeCount++; // Root do not counts
        }
    }
    get isLeaf() {
        return this.children.length == 0;
    }
    get isRoot() {
        return !this.parent;
    }
    get tree() {
        let root = this;
        while (!root.isRoot) {
            root = root.parent;
        }
        return this.model.trees.find(x => x.root == root);
    }
    isParentOf(node) {
        let parent = node.parent;
        while (parent) {
            if (parent == this)
                return true;
            parent = parent.parent;
        }
        return false;
    }
    getSelfObjects() {
        return this.selfObjects;
    }
    getSelfAndChildrenObjects() {
        return this.model.objects.filter(x => {
            return x.nodes.findIndex(node => node == this || this.isParentOf(node)) >= 0;
        });
    }
    getChildContaining(obj) {
        const objects = this.getSelfAndChildrenObjects();
        if (objects.indexOf(obj) >= 0) {
            const containingNodeIndex = obj.nodes.findIndex(x => this.isParentOf(x));
            if (containingNodeIndex >= 0) {
                const containingNode = obj.nodes[containingNodeIndex];
                let node = containingNode;
                while (node.parent != this) {
                    node = node.parent;
                }
                return node;
            }
        }
        return null;
    }
}
exports.Node = Node;
class Tree {
    constructor(model) {
        this.id = utils_1.generateId("tree");
        this.model = model;
    }
    visitAllNodes(current, callback) {
        callback(current);
        if (current.children) {
            current.children.forEach(x => this.visitAllNodes(x, callback));
        }
    }
}
exports.Tree = Tree;
class Model {
    constructor() {
        this.nodeCount = 0;
        this.trees = [];
        this.objects = [];
    }
}
exports.Model = Model;
//# sourceMappingURL=model.js.map