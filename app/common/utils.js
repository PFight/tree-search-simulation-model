"use strict";
var idCounterMap = {};
function generateId(prefix) {
    if (!idCounterMap[prefix]) {
        idCounterMap[prefix] = 0;
    }
    return prefix + idCounterMap[prefix]++;
}
exports.generateId = generateId;
function resetIdGenerator() {
    idCounterMap = {};
}
exports.resetIdGenerator = resetIdGenerator;
//# sourceMappingURL=utils.js.map