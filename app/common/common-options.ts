﻿import { RandomNumber } from "./random-number";
import { Object, Node } from "./model";

export type NodeParamFunc = (node?: Node) => RandomNumber | number;
export type NodeParam = number | NodeParamFunc;
export type Param = number | RandomNumber;

export type IterationParamFunc = (iteration: number) => RandomNumber | number;
export type IterationParam = number | IterationParamFunc;

export type NodeListParamFunc = (node?: Node, objects?: Object[]) => RandomNumber | number;
export type NodeListParam = number | NodeListParamFunc;
export type SelectFromListFunc = (count: number) => RandomNumber | number;
export type SelectFromListParam = RandomNumber | number | SelectFromListFunc

export function getNodeParam(node: Node, option: NodeParam): number {
    if (typeof option == "number") return option as number;
    else {
        let optionFunc = option as NodeParamFunc;
        let result = optionFunc(node);
        if (typeof result == "number") return result as number;
        else {
            return (result as RandomNumber).get();
        }
    }
}

export function getIterationParam(iteration: number, option: IterationParam): number {
    if (typeof option == "number") return option as number;
    else {
        let optionFunc = option as IterationParamFunc;
        let result = optionFunc(iteration);
        if (typeof result == "number") return result as number;
        else {
            return (result as RandomNumber).get();
        }
    }
}

export function getParam(option: Param) {
    if (typeof option == "number") return option as number;
    else {
        return (option as RandomNumber).get();
    }
}


export function getNodeListParam(node: Node, list: Object[], option: NodeListParam): number {
    if (typeof option == "number") return option as number;
    else {
        let optionFunc = option as NodeListParamFunc;
        let result = optionFunc(node, list);
        if (typeof result == "number") return result as number;
        else {
            return (result as RandomNumber).get();
        }
    }
}

export function getSelectFromListParam(count: number, option: SelectFromListParam): number {
    if (typeof option == "number") return option as number;
    else if (typeof option == "function") {
        let optionFunc = option as SelectFromListFunc;
        let result = optionFunc(count);
        if (typeof result == "number") return result as number;
        else {
            return (result as RandomNumber).get();
        }
    }
    else {
        return (option as RandomNumber).get();
    }
}