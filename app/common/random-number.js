"use strict";
const randgen = require("randgen");
class UniformRandomNumber {
    constructor(min, max) {
        this.min = min;
        this.max = max;
    }
    get() {
        return randgen.runif(this.min, this.max);
    }
}
exports.UniformRandomNumber = UniformRandomNumber;
class NormalRandomNumber {
    constructor(med, dev) {
        this.med = med;
        this.dev = dev;
    }
    get() {
        return randgen.rnorm(this.med, this.dev);
    }
}
exports.NormalRandomNumber = NormalRandomNumber;
//# sourceMappingURL=random-number.js.map