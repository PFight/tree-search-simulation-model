"use strict";
function getNodeParam(node, option) {
    if (typeof option == "number")
        return option;
    else {
        let optionFunc = option;
        let result = optionFunc(node);
        if (typeof result == "number")
            return result;
        else {
            return result.get();
        }
    }
}
exports.getNodeParam = getNodeParam;
function getIterationParam(iteration, option) {
    if (typeof option == "number")
        return option;
    else {
        let optionFunc = option;
        let result = optionFunc(iteration);
        if (typeof result == "number")
            return result;
        else {
            return result.get();
        }
    }
}
exports.getIterationParam = getIterationParam;
function getParam(option) {
    if (typeof option == "number")
        return option;
    else {
        return option.get();
    }
}
exports.getParam = getParam;
function getNodeListParam(node, list, option) {
    if (typeof option == "number")
        return option;
    else {
        let optionFunc = option;
        let result = optionFunc(node, list);
        if (typeof result == "number")
            return result;
        else {
            return result.get();
        }
    }
}
exports.getNodeListParam = getNodeListParam;
function getSelectFromListParam(count, option) {
    if (typeof option == "number")
        return option;
    else if (typeof option == "function") {
        let optionFunc = option;
        let result = optionFunc(count);
        if (typeof result == "number")
            return result;
        else {
            return result.get();
        }
    }
    else {
        return option.get();
    }
}
exports.getSelectFromListParam = getSelectFromListParam;
//# sourceMappingURL=common-options.js.map