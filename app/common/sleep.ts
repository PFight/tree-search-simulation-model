﻿var time = 0;

export function sleep(timeout: number) {
    if (timeout > 0) {
        time += timeout;
    }
}

export function now(): number {
    return time;
}

