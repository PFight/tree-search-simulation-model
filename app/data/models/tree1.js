"use strict";
const random_number_1 = require("../../common/random-number");
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = {
    treeCount: 3,
    treeOptions: {
        deep: 2,
        wide: () => new random_number_1.UniformRandomNumber(2, 4)
    },
    objectsOptions: {
        objectCount: 1000,
        maxObjectsPerNode: (node) => node.level < 1 ? 0 : Infinity,
        nodesPerObjectMin: 2,
        nodesPerObjectMax: 3,
        maxTreesPerObject: 1
    }
};
//# sourceMappingURL=tree1.js.map