import { ModelOptions } from "../../generation/model-options";
import { Node } from "../../common/model";
import { UniformRandomNumber } from "../../common/random-number";

export default {
    treeCount: 3,
    nodeCount: 50,
    treeOptions: {
        deep: 2,
        wide: () => new UniformRandomNumber(2, 4)
    },
    objectsOptions: {
        objectCount: 1000,
        maxObjectsPerNode: (node: Node) => node.level < 1 ? 0 : Infinity,
        nodesPerObjectMin: 2,
        nodesPerObjectMax: 3,
        maxTreesPerObject: 1
    }
} as ModelOptions;

