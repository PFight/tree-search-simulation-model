import { ModelOptions } from "../../generation/model-options";
import { Node } from "../../common/model";
import { UniformRandomNumber } from "../../common/random-number";

export default {
    treeCount: 3,
    nodeCount: 50,
    treeOptions: {
        deep: Infinity,
        wide: () => new UniformRandomNumber(3, 7)
    },
    objectsOptions: {
        objectCount: 1000,
        maxObjectsPerNode: (node: Node) => node.level == 0 ? 0 : Infinity,
        nodesPerObjectMin: 1,
        nodesPerObjectMax: 3,
        maxTreesPerObject: Infinity
    }
} as ModelOptions;

