"use strict";
const random_number_1 = require("../common/random-number");
const listDistr = (count) => {
    if (count <= 2)
        return new random_number_1.NormalRandomNumber(800.904762, 217.25619);
    else if (count <= (3 + 5) / 2)
        return new random_number_1.NormalRandomNumber(1161.904762, 217.25619);
    else if (count < (5 + 7) / 2)
        return new random_number_1.NormalRandomNumber(1440.277778, 345.534206);
    else if (count < (7 + 9) / 2)
        return new random_number_1.NormalRandomNumber(1564.722222, 287.699921);
    else if (count < (9 + 11) / 2)
        return new random_number_1.NormalRandomNumber(1844.444444, 300.20254);
    else if (count < (11 + 15) / 2)
        return new random_number_1.NormalRandomNumber(2096.944444, 360.46754);
    else if (count < (15 + 20) / 2)
        return new random_number_1.NormalRandomNumber(2681.111111, 400.090159);
    else if (count < (20 + 30) / 2)
        return new random_number_1.NormalRandomNumber(3078.888889, 450.455873);
    else if (count < (30 + 40) / 2)
        return new random_number_1.NormalRandomNumber(4547.777778, 500.549206);
    else if (count < (40 + 50) / 2)
        return new random_number_1.NormalRandomNumber(5940.277778, 600.53421);
    else if (count > (30 + 40) / 2)
        return new random_number_1.NormalRandomNumber(6746.111111, 700.41302);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = {
    count: 5,
    selectFromFolderTimeout: (node) => listDistr(node.children.length),
    selectFromFolderNotFoundTimeout: (node) => listDistr(node.children.length),
    selectFromListTimeout: (node, list) => listDistr(list.length),
    selectFromListNotFoundTimeout: (node, list) => listDistr(list.length),
    selectMixedFolderAndListTimeout: (node, list) => listDistr(node.children.length + list.length),
    emptyNodeLookupTimeout: () => new random_number_1.NormalRandomNumber(500, 0.345534206),
    maxObjectCountToTryFind: (iter) => new random_number_1.NormalRandomNumber(30 * ((iter > 1) ? iter * 0.3 : 1), 5),
    maxSelectObjectTimeoutBeforeNarrowingSearch: (iter) => new random_number_1.NormalRandomNumber(2000 * iter, 300),
    viewSubfoldersItemsInFolder: false,
};
//# sourceMappingURL=simulation-options.js.map