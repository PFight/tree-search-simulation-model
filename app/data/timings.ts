﻿import { UniformRandomNumber, NormalRandomNumber } from "../common/random-number"; 

export const listDistr = (count: number) => {
    if (count <= 2) return new NormalRandomNumber(800.904762, 217.25619);
    else if (count <= (3 + 5) / 2) return new NormalRandomNumber(1161.904762, 217.25619);
    else if (count < (5 + 7) / 2) return new NormalRandomNumber(1440.277778, 345.534206);
    else if (count < (7 + 9) / 2) return new NormalRandomNumber(1564.722222, 287.699921);
    else if (count < (9 + 11) / 2) return new NormalRandomNumber(1844.444444, 300.20254);
    else if (count < (11 + 15) / 2) return new NormalRandomNumber(2096.944444, 360.46754);
    else if (count < (15 + 20) / 2) return new NormalRandomNumber(2681.111111, 400.090159);
    else if (count < (20 + 30) / 2) return new NormalRandomNumber(3078.888889, 450.455873);
    else if (count < (30 + 40) / 2) return new NormalRandomNumber(4547.777778, 500.549206);
    else if (count < (40 + 50) / 2) return new NormalRandomNumber(5940.277778, 600.53421);
    else return new NormalRandomNumber(2*count*1000/15, 5*count + 350);
};