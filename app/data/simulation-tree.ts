﻿import { SimulationOptions } from "../simulation/simulation-options";
import { UniformRandomNumber, NormalRandomNumber } from "../common/random-number"; 
import { Node, Object } from "../common/model";
import { listDistr } from "./timings";


export default {
    count: 5,
    selectFromListTimeout: (count) => listDistr(count),
    selectFromFolderTimeout: (node: Node) => listDistr(node.children.length),
    selectFromFolderNotFoundTimeout: (node: Node) => listDistr(node.children.length),
    selectFromNodeListTimeout: (node: Node, list: Object[]) => listDistr(list.length),
    selectFromNodeListNotFoundTimeout: (node: Node, list: Object[]) => listDistr(list.length),
    selectMixedFolderAndListTimeout: (node: Node, list: Object[]) => listDistr(node.children.length + list.length),
    emptyNodeLookupTimeout: () => new NormalRandomNumber(500, 0.345534206),
    maxObjectCountToTryFind: (iter) => new NormalRandomNumber(30 * ((iter > 1) ? iter * 0.3 : 1), 5),
    maxSelectObjectTimeoutBeforeNarrowingSearch: (iter) => new NormalRandomNumber(2000*iter, 300),
    showSubfoldersItemsInFolder: false,
} as SimulationOptions;