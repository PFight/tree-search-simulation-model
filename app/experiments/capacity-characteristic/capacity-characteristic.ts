﻿import { generateModel } from "../../generation/model-generator";
import { findRandomObjectTime } from "../../simulation/simulation";
import { ModelOptions } from "../../generation/model-options";
import { Node, Object } from "../../common/model";
import { UniformRandomNumber, NormalRandomNumber } from "../../common/random-number";
import { listDistr } from "../../data/timings";
import { SimulationOptions } from "../../simulation/simulation-options";
import { printModel, newLine } from "../../visualisation/model-print";
import { log, flush } from "../../common/log";
import { IterationParam } from "../../common/common-options";
var fs = require('fs');

// План эксперимента
// Независимые переменные: 
// а) к - во деревьев (1 - 7 с шагом 1) [7 вариантов],
// б) к - во элементов (5 - 50, с шагом 5 [9 вариантов],
// в) к - во объектов ( 20 - 200, c шагом 20, 300 - 1000, с шагом 100 [9+7=16 вариантов]
// Итого, 7*9*16 = 1008 вариантов
// Для каждого варианта выполняем 20 прогонов, получаем 20*1008=20160 прогонов
// Результаты для каждого дерева представляем в виде 
// набора графиков время от к- ва объектов для разного к- ва элементов.

class CapacityCharacteristicModelOptions implements ModelOptions {
    treeCount = undefined;
    nodeCount = undefined;
    treeOptions = {
        deep: Infinity,
        wide: () => new UniformRandomNumber(2, 9)
    };
    objectsOptions = {
        objectCount: undefined,
        maxObjectsPerNode: (node: Node) => node.level < 1 ? 0 : Infinity,
        nodesPerObjectMin: undefined,
        nodesPerObjectMax: undefined,
        maxTreesPerObject: undefined
    }
}

class CapacityCharacteristiSimulationOptions implements SimulationOptions {
    count = 5;
    selectFromListTimeout = (count) => listDistr(count);
    selectFromFolderTimeout = (node: Node) => listDistr(node.children.length);
    selectFromFolderNotFoundTimeout = (node: Node) => listDistr(node.children.length);
    selectFromNodeListTimeout = (node: Node, list: Object[]) => listDistr(list.length);
    selectFromNodeListNotFoundTimeout = (node: Node, list: Object[]) => listDistr(list.length);
    selectMixedFolderAndListTimeout = (node: Node, list: Object[]) => listDistr(node.children.length + list.length);
    emptyNodeLookupTimeout = () => new NormalRandomNumber(500, 0.345534206);
    maxObjectCountToTryFind: IterationParam = (iter) => new NormalRandomNumber(20, 5);
    maxSelectObjectTimeoutBeforeNarrowingSearch: IterationParam = (iter) => new NormalRandomNumber(3000, 300);
    showSubfoldersItemsInFolder = false;
}

class ResultOutput {
    targetFile: string;
    raw: string = "";

    constructor(targetFile: string) {
        this.targetFile = targetFile;
    }

    writeLine(line: string = "") {
        this.raw += line + newLine;
    }
    write(text: string) {
        this.raw += text;
    }

    reset() {
        this.raw = "";
    }

    flush() {
        fs.writeFileSync(this.targetFile, this.raw);
    }
}

export class CapcityCharacteristic {
    private output: ResultOutput;

    public constructor(outFile: string) {
        this.output = new ResultOutput(outFile);
    }

    public get resultOutput() {
        return this.output;
    }
    
    public run() {
        const treeCountMin = 1;
        const treeCountMax = 5;
        const treeCountStep = 1;

        const elementCountMin = 10;
        const elementCountMax = 60;
        const elementCountStep = 15;

        const objectCountMin1 = 50;
        const objectCountMax1 = 3001;
        const objectCountStep1 = 200;

        const objectCountMin2 = 4000;
        const objectCountMax2 = 31000;
        const objectCountStep2 = 3000;

        const iterationCount = 50;

        for (let treeCount = treeCountMin; treeCount <= treeCountMax; treeCount += treeCountStep) {
            console.info("Tree count: " + treeCount);
            // Write header
            this.output.writeLine();
            this.output.writeLine();
            this.output.writeLine("Tree count: " + treeCount);
            this.output.writeLine();
            this.output.write("Element count \ object count,");
            for (let objectCount = objectCountMin1; objectCount <= objectCountMax1; objectCount += objectCountStep1) {
                this.output.write(objectCount.toString() + ", ");
            }
            for (let objectCount = objectCountMin2; objectCount <= objectCountMax2; objectCount += objectCountStep2) {
                this.output.write(objectCount.toString() + ", ");
            }
            this.output.writeLine();

            // Iterate element count
            for (let elementCount = elementCountMin; elementCount <= elementCountMax; elementCount += elementCountStep) {
                console.info("  Element count: " + elementCount);
                this.output.write(elementCount + ", ");
                // Iterate object count
                for (let objectCount = objectCountMin1; objectCount <= objectCountMax1; objectCount += objectCountStep1) {
                    console.info("    Object count: " + objectCount);
                    let result = this.runStep(treeCount, elementCount, objectCount, iterationCount);
                    this.output.write(result + ", ");
                    if (result > 20)
                        break;
                }
                for (let objectCount = objectCountMin2; objectCount <= objectCountMax2; objectCount += objectCountStep2) {
                    console.info("    Object count: " + objectCount);
                    let result = this.runStep(treeCount, elementCount, objectCount, iterationCount);
                    this.output.write(result + ", ");
                    if (result > 20)
                        break;
                }
                this.output.writeLine();
                this.output.flush();
            }
            flush();
        }

        this.output.flush();
    }

    private runStep(treeCount: number, elementCount: number, objectCount: number, iterationCount: number): number {
        // Generate model
        let modelOptions = new CapacityCharacteristicModelOptions();
        modelOptions.treeCount = treeCount;
        modelOptions.nodeCount = elementCount;
        modelOptions.objectsOptions.objectCount = objectCount;
        modelOptions.objectsOptions.nodesPerObjectMin = Math.min(treeCount, 3);
        modelOptions.objectsOptions.nodesPerObjectMax = Math.min(treeCount, 3);
        modelOptions.objectsOptions.maxTreesPerObject = 1;
        let model = generateModel(modelOptions);
        log(printModel(model));

        // Simulate
        let simulationOptions = new CapacityCharacteristiSimulationOptions();
        if (treeCount == 1) {
            simulationOptions.maxObjectCountToTryFind = Infinity;
            simulationOptions.maxSelectObjectTimeoutBeforeNarrowingSearch = Infinity;
        }
        let summTime = 0;
        for (let i = 0; i < iterationCount; i++) {
            let result = findRandomObjectTime(model, simulationOptions);
            log(newLine + "Time spent: " + result.timeSpentSec + " sec. " + newLine);
            summTime += result.timeSpentSec;
        }
        let avgTime = summTime / iterationCount;
        return avgTime;
    }
}
