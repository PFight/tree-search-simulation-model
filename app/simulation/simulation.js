"use strict";
const randgen = require("randgen");
const sleep_1 = require("../common/sleep");
const common_options_1 = require("../common/common-options");
const log_1 = require("../common/log");
const model_print_1 = require("../visualisation/model-print");
function findRandomObjectTime(model, options) {
    const sim = new SearchObjectSimulation(model, options);
    sim.perform();
    return sim;
}
exports.findRandomObjectTime = findRandomObjectTime;
function sleepParam(param) {
    sleep_1.sleep(common_options_1.getParam(param));
}
function sleepNodeParam(node, param) {
    sleep_1.sleep(common_options_1.getNodeParam(node, param));
}
function sleepNodeListParam(node, list, param) {
    sleep_1.sleep(common_options_1.getNodeListParam(node, list, param));
}
class SearchObjectSimulation {
    constructor(model, options) {
        this.selectedNodes = [];
        this.filteredObjects = [];
        this.model = model;
        this.options = options;
    }
    perform() {
        this.startTime = sleep_1.now();
        this.filteredObjects = this.model.objects;
        this.objectToFind = randgen.rlist(this.model.objects);
        log_1.log(model_print_1.newLine + model_print_1.newLine + "Ищем объект " + model_print_1.objectToString(this.objectToFind) + model_print_1.newLine);
        this.found = false;
        this.iteration = 1;
        while (!this.found) {
            let searchHistory = [];
            let bestHistory = 0;
            for (let i = 0; i < this.model.trees.length && !this.found; i++) {
                // Ищем подходящее дерево
                if (this.model.trees[i].root.getSelfAndChildrenObjects().find(x => x == this.objectToFind) != null) {
                    // Выбираем дерево
                    let selectTreeTime = common_options_1.getSelectFromListParam(this.model.trees.length, this.options.selectFromListTimeout);
                    log_1.log("Выбираем дерево " + this.model.trees[i].id + " " + selectTreeTime / 1000 + "сек. " + model_print_1.newLine);
                    sleep_1.sleep(selectTreeTime);
                    this.found = this.findInTree(this.model.trees[i].root, this.objectToFind);
                    let selectedNodes = this.selectedNodes.map(x => x);
                    searchHistory.push({
                        count: this.filteredObjects.length,
                        nodes: selectedNodes
                    });
                    if (searchHistory[bestHistory].count > this.filteredObjects.length) {
                        bestHistory = searchHistory.length - 1;
                    }
                }
            }
            if (!this.found) {
                this.selectedNodes = [];
                this.filteredObjects = this.model.objects;
                let nodes = searchHistory[bestHistory].nodes;
                for (let key in nodes) {
                    this.selectNode(nodes[key]);
                }
                let selectTime = common_options_1.getSelectFromListParam(this.filteredObjects.length, this.options.selectFromListTimeout);
                log_1.log("Влегкую найти не получилось, берем лучшую комбинацию (" + this.filteredObjects.length + " объектов) и ищем пока не найдем - " + selectTime / 1000 + "сек. " + model_print_1.newLine);
                sleep_1.sleep(selectTime);
                this.found = this.filteredObjects.indexOf(this.objectToFind) >= 0;
            }
        }
        this.finishTime = sleep_1.now();
    }
    get timeSpentSec() {
        return (this.finishTime - this.startTime) / 1000;
    }
    selectNode(node) {
        this.selectedNodes.push(node);
        this.filteredObjects = this.filteredObjects.filter(x => {
            return x.nodes.indexOf(node) >= 0;
        });
    }
    findInTree(node, obj) {
        log_1.log(model_print_1.nodeToString(node));
        let ident = model_print_1.getIdent(node.level);
        let nodeSelfObjects = this.filteredObjects.filter(x => x.nodes.indexOf(node) >= 0);
        if (node.children.length == 0 && nodeSelfObjects.length == 0) {
            sleepNodeParam(node, this.options.emptyNodeLookupTimeout);
        }
        if (nodeSelfObjects.length > 0) {
            let tooManyObjects = nodeSelfObjects.length > common_options_1.getIterationParam(this.iteration, this.options.maxObjectCountToTryFind);
            let containsObjSelf = nodeSelfObjects.indexOf(obj) >= 0;
            let maxSearchTime = common_options_1.getIterationParam(this.iteration, this.options.maxSelectObjectTimeoutBeforeNarrowingSearch);
            let searchTime;
            if (!tooManyObjects) {
                if (containsObjSelf) {
                    searchTime = common_options_1.getNodeListParam(node, nodeSelfObjects, this.options.selectFromNodeListTimeout);
                }
                else {
                    searchTime = 0; // getNodeListParam(node, nodeSelfObjects, this.options.selectFromNodeListNotFoundTimeout);
                }
                ;
                sleep_1.sleep(Math.min(searchTime, maxSearchTime));
            }
            if (!tooManyObjects && searchTime < maxSearchTime) {
                if (containsObjSelf) {
                    log_1.log(ident + "- ищем среди " + nodeSelfObjects.length + " объектов " + model_print_1.timeToString(searchTime) + " и находим" + model_print_1.newLine);
                    return true;
                }
                else {
                    log_1.log(ident + "- ищем среди " + nodeSelfObjects.length + " объектов " + model_print_1.timeToString(searchTime) + " - объекта здесь нет" + model_print_1.newLine);
                }
            }
            else {
                if (containsObjSelf) {
                    if (tooManyObjects) {
                        log_1.log(ident + "- ищем среди " + nodeSelfObjects.length + " объектов - слишком много объектов. Выделяем элемент." + model_print_1.newLine);
                    }
                    else {
                        log_1.log(ident + "- ищем среди " + nodeSelfObjects.length + " объектов " + model_print_1.timeToString(searchTime) + "/" + model_print_1.timeToString(maxSearchTime) + " но не хватает терпения. Выделяем элемент." + model_print_1.newLine);
                    }
                    this.selectNode(node);
                    return false;
                }
                else {
                    log_1.log(ident + "Объекта здесь нет." + model_print_1.newLine);
                }
            }
        }
        if (node.children.length > 0) {
            let selectedChild = node.getChildContaining(obj);
            if (selectedChild) {
                let selectTime = common_options_1.getNodeParam(node, this.options.selectFromFolderTimeout);
                sleep_1.sleep(selectTime);
                log_1.log(ident + "- выбираем из " + node.children.length + " дочерних элементов " + model_print_1.timeToString(selectTime) + " Выбрали." + model_print_1.newLine);
                return this.findInTree(selectedChild, obj);
            }
            else {
                let selectTime = common_options_1.getNodeParam(node, this.options.selectFromFolderNotFoundTimeout);
                sleep_1.sleep(selectTime);
                log_1.log(ident + "- выбираем из " + node.children.length + " дочерних элементов " + model_print_1.timeToString(selectTime) + " Нет ничего подходящего." + model_print_1.newLine);
                return false;
            }
        }
        else {
            return false;
        }
    }
}
exports.SearchObjectSimulation = SearchObjectSimulation;
//# sourceMappingURL=simulation.js.map