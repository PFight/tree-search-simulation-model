﻿const randgen = require("randgen");
import { sleep, now } from "../common/sleep";
import { Model, Node, Object } from "../common/model";
import { SimulationOptions } from "./simulation-options";
import { getNodeParam, getParam, Param, NodeParam, getIterationParam,
    NodeListParam, getNodeListParam, getSelectFromListParam } from "../common/common-options";
import { log } from "../common/log";
import { newLine, objectToString, nodeToString, timeToString, getIdent } from "../visualisation/model-print";

export function findRandomObjectTime(model: Model, options: SimulationOptions): SearchObjectSimulation {
    const sim = new SearchObjectSimulation(model, options);
    sim.perform();
    return sim;
}

function sleepParam(param: Param) {
    sleep(getParam(param));
}
function sleepNodeParam(node: Node, param: NodeParam) {
    sleep(getNodeParam(node, param));
}
function sleepNodeListParam(node: Node, list: Object[], param: NodeListParam) {
    sleep(getNodeListParam(node, list, param));
}

export class SearchObjectSimulation {
    model: Model;
    options: SimulationOptions;
    selectedNodes: Node[] = [];
    filteredObjects: Object[] = [];
    objectToFind: Object;
    startTime: number;
    finishTime: number;
    found: boolean;
    iteration: number;

    public constructor(model: Model, options: SimulationOptions) {
        this.model = model;
        this.options = options;
    }

    public perform(): void {
        this.startTime = now();
        this.filteredObjects = this.model.objects;
        this.objectToFind = randgen.rlist(this.model.objects);
        log(newLine + newLine + "Ищем объект " + objectToString(this.objectToFind) + newLine);
        this.found = false;
        this.iteration = 1;
        while (!this.found) {
            let searchHistory = [];
            let bestHistory = 0;
            for (let i = 0; i < this.model.trees.length && !this.found; i++) {
                // Ищем подходящее дерево
                if (this.model.trees[i].root.getSelfAndChildrenObjects().find(x => x == this.objectToFind) != null) {
                    // Выбираем дерево
                    let selectTreeTime = getSelectFromListParam(this.model.trees.length, this.options.selectFromListTimeout);
                    log("Выбираем дерево " + this.model.trees[i].id + " " + selectTreeTime/1000 + "сек. " + newLine);
                    sleep(selectTreeTime);
                    this.found = this.findInTree(this.model.trees[i].root, this.objectToFind);

                    let selectedNodes = this.selectedNodes.map(x => x);
                    searchHistory.push({
                        count: this.filteredObjects.length,
                        nodes: selectedNodes
                    });
                    if (searchHistory[bestHistory].count > this.filteredObjects.length) {
                        bestHistory = searchHistory.length - 1;
                    }
                }
            }
            if (!this.found) {
                this.selectedNodes = [];
                this.filteredObjects = this.model.objects;
                let nodes = searchHistory[bestHistory].nodes;
                for (let key in nodes) {
                    this.selectNode(nodes[key]);
                }
                let selectTime = getSelectFromListParam(this.filteredObjects.length, this.options.selectFromListTimeout);
                log("Влегкую найти не получилось, берем лучшую комбинацию (" + this.filteredObjects.length +" объектов) и ищем пока не найдем - " + selectTime/1000 + "сек. " + newLine);
                sleep(selectTime);
                this.found = this.filteredObjects.indexOf(this.objectToFind) >= 0;
            }
        }

        this.finishTime = now();
    }

    public get timeSpentSec() {
        return (this.finishTime - this.startTime) / 1000;
    }

    private selectNode(node: Node) {
        this.selectedNodes.push(node);
        this.filteredObjects = this.filteredObjects.filter(x => {
            return x.nodes.indexOf(node) >= 0;
        });
    }

    private findInTree(node: Node, obj: Object): boolean {
        log(nodeToString(node));
        let ident = getIdent(node.level);
        let nodeSelfObjects = this.filteredObjects.filter(x => x.nodes.indexOf(node) >= 0);
        if (node.children.length == 0 && nodeSelfObjects.length == 0) {
            sleepNodeParam(node, this.options.emptyNodeLookupTimeout);
        }
        if (nodeSelfObjects.length > 0) {
            let tooManyObjects = nodeSelfObjects.length > getIterationParam(this.iteration, this.options.maxObjectCountToTryFind);
            let containsObjSelf = nodeSelfObjects.indexOf(obj) >= 0;
            let maxSearchTime = getIterationParam(this.iteration, this.options.maxSelectObjectTimeoutBeforeNarrowingSearch)
            let searchTime;
            if (!tooManyObjects) {
                if (containsObjSelf) {
                    searchTime = getNodeListParam(node, nodeSelfObjects, this.options.selectFromNodeListTimeout);

                } else {
                    searchTime = 0; // getNodeListParam(node, nodeSelfObjects, this.options.selectFromNodeListNotFoundTimeout);
                }
                ;
                sleep(Math.min(searchTime, maxSearchTime));
            }
            if (!tooManyObjects && searchTime < maxSearchTime) {
                if (containsObjSelf) {
                    log(ident + "- ищем среди " + nodeSelfObjects.length + " объектов " + timeToString(searchTime) + " и находим" + newLine);
                    return true;
                } else {
                    log(ident + "- ищем среди " + nodeSelfObjects.length + " объектов " + timeToString(searchTime) + " - объекта здесь нет" + newLine);
                }
            } else {
                if (containsObjSelf) {
                    if (tooManyObjects) {
                        log(ident + "- ищем среди " + nodeSelfObjects.length + " объектов - слишком много объектов. Выделяем элемент." + newLine);
                    } else {
                        log(ident + "- ищем среди " + nodeSelfObjects.length + " объектов " + timeToString(searchTime) + "/" + timeToString(maxSearchTime) + " но не хватает терпения. Выделяем элемент." + newLine);
                    }
                    this.selectNode(node);
                    return false;
                } else {
                    log(ident + "Объекта здесь нет." + newLine);
                }
            }
            
        }
        if (node.children.length > 0) {
            let selectedChild = node.getChildContaining(obj);
            if (selectedChild) {
                let selectTime = getNodeParam(node, this.options.selectFromFolderTimeout);
                sleep(selectTime);
                log(ident + "- выбираем из " + node.children.length + " дочерних элементов " + timeToString(selectTime) + " Выбрали." + newLine);
                return this.findInTree(selectedChild, obj);
            } else {
                let selectTime = getNodeParam(node, this.options.selectFromFolderNotFoundTimeout);
                sleep(selectTime);
                log(ident + "- выбираем из " + node.children.length + " дочерних элементов " + timeToString(selectTime) + " Нет ничего подходящего." + newLine);
                return false;
            }
        } else {
            return false;
        }
    }


}

