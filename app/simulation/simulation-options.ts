﻿let randgen = require("randgen");
import { RandomNumber } from "../common/random-number";
import { NodeParam, Param, IterationParam, SelectFromListParam, NodeListParam } from "../common/common-options";

export interface SimulationOptions {
    count: number;
    selectFromListTimeout: SelectFromListParam,
    selectFromFolderTimeout: NodeParam;
    selectFromFolderNotFoundTimeout: NodeParam;
    selectFromNodeListTimeout: NodeListParam;
    selectFromNodeListNotFoundTimeout: NodeListParam;
    emptyNodeLookupTimeout: NodeParam;
    maxObjectCountToTryFind: IterationParam;
    maxSelectObjectTimeoutBeforeNarrowingSearch: IterationParam;
    showSubfoldersItemsInFolder: boolean;
}