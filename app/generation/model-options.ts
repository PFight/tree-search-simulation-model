﻿import { NodeParam } from "../common/common-options";

export interface TreeOptions {
    deep: NodeParam;
    wide: NodeParam;
}

export class ObjectDistributions {
    public static allNodes = "allNodes";
}

export interface ObjectsOptions {
    objectCount: number;
    maxObjectsPerNode: NodeParam;
    nodesPerObjectMin: number;
    nodesPerObjectMax: number;
    maxTreesPerObject: number;
}

export interface ModelOptions {
    treeCount: number;
    nodeCount: number;
    treeOptions: TreeOptions;
    objectsOptions: ObjectsOptions;
}