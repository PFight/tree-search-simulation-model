import { ModelOptions, ObjectDistributions } from "./model-options";
import { NodeParam, NodeParamFunc, getNodeParam } from "../common/common-options";
import { RandomNumber } from "../common/random-number";
import { Model, Tree, Node, Object } from "../common/model";
import { resetIdGenerator } from "../common/utils";

export function generateModel(options: ModelOptions): Model {
    resetIdGenerator();
    let model = new Model();
    model.trees = generateTrees(model, options);
    model.objects = generateObjects(model.trees, options);
    return model;
}

function generateTrees(model: Model, options: ModelOptions): Tree[] {
    let result: Tree[] = [];
    for (let i = 0; i < options.treeCount; i++) {
        let tree = new Tree(model);
        tree.root = new Node(model, 0);
        result.push(tree);
    }
    while (model.nodeCount < options.nodeCount) {
        for (let i = 0; i < result.length; i++) {
            let tree = result[i];
            generateNextLevel(tree.root, 1, options, model);
        }
    }
    return result;
}

function generateNextLevel(currentNode: Node, currentLevel: number, options: ModelOptions, model: Model) {
    let deep = getNodeParam(currentNode, options.treeOptions.deep);
    if (currentNode.children.length == 0 && currentLevel <= deep) {
        let count = getNodeParam(currentNode, options.treeOptions.wide);
        if (count > options.nodeCount / options.treeCount) {
            count = Math.max(options.nodeCount / options.treeCount, 1);
        }
        for (let n = 0; n < count && model.nodeCount < options.nodeCount; n++) {
            var levelNode = new Node(model, currentLevel, currentNode);
        }
    } else {
        let leafNodes = currentNode.children.filter(x => x.children.length == 0);
        if (leafNodes.length > 0) {
            let someLeafInd = getRandomInt(0, leafNodes.length - 1);
            generateNextLevel(leafNodes[someLeafInd], currentLevel + 1, options, model);
        } else {
            for (let n = 0; n < currentNode.children.length; n++) {
                if (model.nodeCount < options.nodeCount) {
                    generateNextLevel(currentNode.children[n], currentLevel + 1, options, model);
                }
            }
        }
    }
}

function generateObjects(trees: Tree[], options: ModelOptions): Object[] {
    let objects: Object[] = new Array(options.objectsOptions.objectCount);
    let workObjects: Object[] = new Array(options.objectsOptions.objectCount);
    for (let i = 0; i < options.objectsOptions.objectCount; i++) {
        let obj = new Object();
        objects[i] = obj;
        workObjects[i] = obj;
    }
    let nodes: Node[] = new Array();
    trees.forEach(tree => {
        tree.visitAllNodes(tree.root, (node) => {
            nodes.push(node);
        });
    });
    let distributionOk = () => {
        let middle = (options.objectsOptions.nodesPerObjectMax + options.objectsOptions.nodesPerObjectMax) / 2;
        let lessMiddleCount = 0;
        let greaterMiddleCount = 0;
        let middleCount = 0;
        objects.forEach(x => {
            if (x.nodes.length < middle) lessMiddleCount++;
            else if (x.nodes.length > middle) greaterMiddleCount++;
            else middleCount++;
        });
        return lessMiddleCount <= greaterMiddleCount;
    };
    while (!distributionOk()) {
        for (let i = 0; i < workObjects.length / 3; i++) {
            let objectIndex = getRandomInt(0, workObjects.length - 1);
            let object = workObjects[objectIndex];
            if (object.nodes.length < options.objectsOptions.nodesPerObjectMax) {
                let addedToSomeNode = false;
                while (!addedToSomeNode) {
                    let nodeIndex = getRandomInt(0, nodes.length - 1);
                    let node = nodes[nodeIndex];
                    let nodeObjects = node.selfObjects.length;
                    if (object.nodes.indexOf(node) < 0 &&
                        nodeObjects < getNodeParam(node, options.objectsOptions.maxObjectsPerNode) &&
                        object.nodes.filter(x => x.tree == node.tree).length < options.objectsOptions.maxTreesPerObject) {
                        object.nodes.push(node);
                        node.selfObjects.push(object);
                        addedToSomeNode = true;
                    }
                }
            } else {
                workObjects.splice(objectIndex, 1);
            }
        }
    }
    return objects;
}




// ���������� ��������� ����� ����� ����� min (������������) � max (�� ������� max)
// ������������� ������ Math.round() ���� ��� ������������� �������������!
function getRandomInt(min: number, max: number): number {
    return Math.round(Math.random() * (max - min)) + min;
}