﻿import { generateModel } from "./generation/model-generator";
import { printModel } from "./visualisation/model-print";
import { findRandomObjectTime } from "./simulation/simulation";
import { log, flush } from "./common/log";
import { CapcityCharacteristic } from "./experiments/capacity-characteristic/capacity-characteristic";

let experiment = new CapcityCharacteristic("result.csv");
experiment.run();

flush();

//import * as tree1 from "./data/models/multi-tree";
//import * as tree2 from "./data/models/tree";

//let tree = tree1;

//var model = generateModel(tree.default);

//log(printModel(model));

//import * as simoptionsMulti from "./data/simulation-multi-tree";
//import * as simoptionsTree from "./data/simulation-tree";
//for (let i = 0; i < simoptionsMulti.default.count; i++) {
//    let sim = findRandomObjectTime(model, simoptionsMulti.default);
//    log("Затрачено всего " + sim.timeSpentSec + " сек.\n");
//    log("-----------------------------------------------------\n");
//    log("-----------------------------------------------------\n");
//}

//flush();